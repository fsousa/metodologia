package model;

import java.io.IOException;
import java.util.Scanner;

public class Mergesort {

	public static void mergesort(final String[] dados, final int inicio, int n) {
		final int n1;
		final int n2;

		if (n > 1) {
			n1 = n / 2;
			n2 = n - n1;
			mergesort(dados, inicio, n1);
			mergesort(dados, inicio + n1, n2);

			Merge.merge(dados, inicio, n1, n2);
		}
	}

	public static void main(String[] args) {
		Entrada input = null;
		Saida output = null;
		String quebraLinha = System.getProperty("line.separator");
		String[] frases = null;
		int tamanhoParamentros = args.length;
		
		if(tamanhoParamentros == 1){
			output = new Saida(args[0]);
		}else if(tamanhoParamentros == 2){
			input = new Entrada(args[0]);
			output = new Saida(args[1]);
		}else if(tamanhoParamentros > 2){
			System.out.println("Parametros invalidos");
			System.exit(1);
		}
		
		
		if(input == null){
			Scanner scan = new Scanner(System.in);
	        System.out.println("Entrada: ");
	        String linhas = "";
	        while (scan.hasNextLine()) {
	        	String linha = scan.nextLine().trim();
	        	if(linha.equals(""))
	        		break;
	            linhas += linha+quebraLinha;
	        }
	        scan.close();
	        
	        frases = linhas.split(quebraLinha);
	        
		}else{
			try {
				frases = input.getLinhas();
			} catch (IOException e) {
				System.out.println("Arquivo de entrada nao existe.");
				System.exit(1);
			}
		}
		
		for (int i = 0; i < frases.length; i++) {
			String linha = frases[i];
			String[] palavras = linha.split(" ");
			mergesort(palavras, 0, palavras.length);
			linha = "";
			for (String palavra : palavras) {
				linha += palavra + " ";
			}
			frases[i] = linha.trim();
		}
		
		mergesort(frases, 0, frases.length);
		
		if(output == null){
			System.out.println("Saida: ");
			for (String string : frases) {
			    System.out.println(string);
		    }
		}else{
			try {
				output.salvarLinhas(frases);
			} catch (IOException e) {
				System.out.println("Arquivo de saida nao existe.");
				System.exit(1);
			}
		}

	}

}
