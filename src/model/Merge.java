package model;

public class Merge {
	
	public static void merge(String[] dados, int inicio, int n1, int n2) {
		String[] temp = new String[n1 + n2];
		int copiado = 0;
		int copiadoMetade1 = 0;
		int copiadoMetade2 = 0;
		int i;


		while ((copiadoMetade1 < n1) && (copiadoMetade2 < n2)) {
			if (dados[inicio + copiadoMetade1].compareTo(dados[inicio + n1 + copiadoMetade2]) < 0)
				temp[copiado++] = dados[inicio + (copiadoMetade1++)];
			else
				temp[copiado++] = dados[inicio + n1 + (copiadoMetade2++)];
		}

		while (copiadoMetade1 < n1)
			temp[copiado++] = dados[inicio + (copiadoMetade1++)];
		while (copiadoMetade2 < n2)
			temp[copiado++] = dados[inicio + n1 + (copiadoMetade2++)];

		for (i = 0; i < n1 + n2; i++)
			dados[inicio + i] = temp[i];
	}
}
