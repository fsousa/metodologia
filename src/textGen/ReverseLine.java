package textGen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;

public class ReverseLine {

	private static String name = "src/textGen/rev/BDout3.txt";
	private static String save = "src/textGen/rev/BDout30.txt";
	private static Writer writer = null;

	public static void main(String[] args) {
		try {
			FileReader file = new FileReader(name);
			BufferedReader bf = new BufferedReader(file);
			
			String s = null;
			try {
				writer = new BufferedWriter(new FileWriter(save));
				while ((s = bf.readLine()) != null) {
					String[] splited = s.split(" ");
					Arrays.sort(splited, Collections.reverseOrder());
					String result = "";
					for (String str : splited) {
						result = result.concat(str + " ");
					}
					writeOut(result);

				}
				writer.close();
				bf.close();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void writeOut(String str) {
		try {
			writer.write(str + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
