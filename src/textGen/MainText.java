package textGen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainText {

	private static String bdFile = "src/textGen/BD.txt";
	private static String outText = "src/TextGen/BDout3.txt";
	private static List<String> bdList = new ArrayList<String>();
	private static Writer writer = null;

	public static void main(String[] args) {
		try {
			writer = new BufferedWriter(new FileWriter(outText, true));
			readBD();

			Random rd = new Random();
			int rand;
			for (int i = 0; i < 1000; i++) {
				rand = rd.nextInt(50);
				for (int j = 0; j < rand; j++) {
					int myRand = rd.nextInt(bdList.size() - 1);
					writeOut(bdList.get(myRand), true);
				}
				writeOut(bdList.get(rand), false);
			}

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void readBD() {
		FileReader f = null;
		try {
			f = new FileReader(bdFile);
			BufferedReader br = new BufferedReader(f);

			String s;
			try {
				while ((s = br.readLine()) != null) {
					bdList.add(s);
				}
				br.close();
				f.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void writeOut(String str, Boolean space) {
		try {
			if (space) {
				writer.write(str + " ");
			} else
				writer.write(str + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
