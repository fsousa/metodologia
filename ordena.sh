#! /bin/bash



while getopts a:i:o:t: OPCAO; do
     case "${OPCAO}" in
     	a) ALGORITHM=${OPTARG} ;;
        i) INPUTFILE=${OPTARG} ;;
        o) OUTPUTFILE=${OPTARG} ;;
        t) TLIMIT=${OPTARG} ;;
        ?) echo "Algo de errado aconteceu aqui. =( . Por favor leia o arquivo README."; exit 1; ;;
     esac
 done
  
if [ -z $ALGORITHM ] 
then
	echo "Algorithm value is required. See README file."
	exit
fi 
  
# --- Clean
#rm -rf ./bin/*

# --- Build
#echo "Compiling files"
#javac -d ./bin ./src/*/*.java

# --- Run
if [ ! -z $INPUTFILE ] 
then
	if [ ! -f $INPUTFILE ]
	then 
		echo "Input file does not exist."
		exit
	fi
	#cp -R $INPUTFILE bin/
fi

#cd bin
echo $INPUTFILE $OUTPUTFILE
case $ALGORITHM in 
1)
	java -cp bin/ -Xmx2048m model/Mergesort $INPUTFILE $OUTPUTFILE
;;
2)
	java -cp bin/ -Xmx2048m model/MergeSortThread $INPUTFILE $OUTPUTFILE
;;
3)
	java -cp bin/ -Xmx2048m model/MergeSortThreadLimit $INPUTFILE $OUTPUTFILE $TLIMIT
;;
*)
	echo "Wrong usage. Please, open README file and check." 
	exit
;;
esac

if [ ! -z $OUTPUTFILE ] 
then
	cp $OUTPUTFILE ../
    rm $OUTPUTFILE
fi
