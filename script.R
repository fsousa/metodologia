# TAMANHO-TIPO_ARQUIVO-ALGORITMO

data1 = read.table('src/newTimeFormat/pck1/1-ord-1.txt.final')
data2 = read.table('src/newTimeFormat/pck1/1-ord-3.txt.final')
data3 = read.table('src/newTimeFormat/pck1/1-rev-1.txt.final')
data4 = read.table('src/newTimeFormat/pck1/1-rev-3.txt.final')
data5 = read.table('src/newTimeFormat/pck1/3-ord-1.txt.final')
data6 = read.table('src/newTimeFormat/pck1/3-ord-3.txt.final')
data7 = read.table('src/newTimeFormat/pck1/3-rev-1.txt.final')
data8 = read.table('src/newTimeFormat/pck1/3-rev-3.txt.final')
data9 = read.table('src/newTimeFormat/pck2/1-ord-1.txt.final')
data10 = read.table('src/newTimeFormat/pck2/1-ord-3.txt.final')
data11 = read.table('src/newTimeFormat/pck2/1-rev-1.txt.final')
data12 = read.table('src/newTimeFormat/pck2/1-rev-3.txt.final')
data13 = read.table('src/newTimeFormat/pck2/3-ord-1.txt.final')
data14 = read.table('src/newTimeFormat/pck2/3-ord-3.txt.final')
data15 = read.table('src/newTimeFormat/pck2/3-rev-1.txt.final')
data16 = read.table('src/newTimeFormat/pck2/3-rev-3.txt.final')


m_data1 = colMeans(data1)
m_data2 = colMeans(data2)
m_data3 = colMeans(data3)
m_data4 = colMeans(data4)
m_data5 = colMeans(data5)
m_data6 = colMeans(data6)
m_data7 = colMeans(data7)
m_data8 = colMeans(data8)
m_data9 = colMeans(data9)
m_data10 = colMeans(data10)
m_data11 = colMeans(data11)
m_data12 = colMeans(data12)
m_data13 = colMeans(data13)
m_data14 = colMeans(data14)
m_data15 = colMeans(data15)
m_data16 = colMeans(data16)

I = c(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
A = c(-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1)
B = c(-1,-1,-1,-1,1,1,1,1,-1,-1,-1,-1,1,1,1,1)
C = c(-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1)
D = c(-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1)
Y = c(m_data1, m_data2, m_data3, m_data4, m_data5, m_data6, m_data7, m_data8, m_data9, m_data10, m_data11, m_data12, m_data13, m_data14, m_data15, m_data16)

AB = A*B
AC = A*C
AD = A*D
BC = B*C
BD = B*D
CD = C*D
ABC = A*B*C
ABD = A*B*D
ACD = A*C*D
BCD = B*C*D
ABCD = A*B*C*D

media = mean(I*Y)
mediaA = mean(A*Y)
mediaB = mean(B*Y)
mediaC = mean(C*Y)
mediaD = mean(D*Y)
mediaAB = mean(A*B*Y)
mediaAC = mean(A*C*Y)
mediaAD = mean(A*D*Y)
mediaBC = mean(B*C*Y)
mediaBD = mean(B*D*Y)
mediaCD = mean(C*D*Y)
mediaABC = mean(A*B*C*Y)
mediaABD = mean(A*B*D*Y)
mediaACD = mean(A*C*D*Y)
mediaBCD = mean(B*C*D*Y)
mediaABCD = mean(A*B*C*D*Y)

erroA = (mediaA **2) * 16 
erroB = (mediaB **2) * 16 
erroC = (mediaC **2) * 16 
erroD = (mediaD **2) * 16 
erroAB = (mediaAB ** 2) * 16
erroAC = (mediaAC ** 2) * 16
erroAD = (mediaAD ** 2) * 16
erroBC = (mediaBC ** 2) * 16
erroBD = (mediaBD ** 2) * 16
erroCD = (mediaCD ** 2) * 16
erroABC = (mediaABC ** 2) * 16
erroABD = (mediaABD ** 2) * 16
erroACD = (mediaACD ** 2) * 16
erroBCD = (mediaBCD ** 2) * 16
erroABCD = (mediaABCD ** 2) * 16

SQE = erroA + erroB + erroC + erroD + erroAB + erroAC + erroAD + erroBC + erroBD + erroCD + erroABC + erroABD + erroACD +  erroBCD + erroABCD

varA = ( erroA / SQE) * 100
varB = ( erroB / SQE) * 100
varC = ( erroC / SQE) * 100
varD = ( erroD / SQE) * 100
varAB = ( erroAB / SQE) * 100
varAC = ( erroAC / SQE) * 100
varAD = ( erroAD / SQE) * 100
varBC = ( erroBC / SQE) * 100
varBD = ( erroBD / SQE) * 100
varCD =  ( erroCD / SQE) * 100
varABC = ( erroABC / SQE) * 100
varABD = ( erroABD / SQE) * 100
varACD = ( erroACD / SQE) * 100
varBCD = ( erroBCD / SQE) * 100
varABCD = ( erroABCD / SQE) * 100

print(paste("A",varA))
print(paste("B",varB))
print(paste("C",varC))
print(paste("D",varD))
print(paste("AB",varAB))
print(paste("AC",varAC))
print(paste("AD",varAD))
print(paste("BC",varBC))
print(paste("BD",varBD))
print(paste("CD",varCD))
print(paste("ABC",varABC))
print(paste("ABD",varABD))
print(paste("ACD",varACD))
print(paste("BCD",varBCD))
print(paste("ABCD",varABCD))

pieData = c(round(varA,digits=2),round(varB,digits=2),round(varD,digits=2),round(varAB,digits=2), 0.312)
pieLabels = c("A", "B", "D", "AB", "Outros")
pieLabels = paste(pieLabels, pieData)
pieLabels = paste(pieLabels, "%", sep="")
pie(pieData, labels=pieLabels)